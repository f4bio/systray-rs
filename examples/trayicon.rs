extern crate systray;

use std::env;
use systray::SystrayError;

#[cfg(target_os = "linux")]
fn main() {
  let mut app;
  match systray::Application::new() {
    Ok(w) => app = w,
    Err(_) => panic!("Can't create window!"),
  }
  app
    .set_icon_from_file(&"/usr/share/gxkb/flags/ua.png".to_string())
    .ok();
  app
    .add_menu_item(&"Print a thing".to_string(), |_| {
      println!("Printing a thing!");
    })
    .ok();
  app
    .add_menu_item(&"Add Menu Item".to_string(), |window| {
      window
        .add_menu_item(&"Interior item".to_string(), |_| {
          println!("what");
        })
        .ok();
      window.add_menu_separator().ok();
    })
    .ok();
  app.add_menu_separator().ok();
  app
    .add_menu_item(&"Quit".to_string(), |window| {
      window.quit();
    })
    .ok();
  println!("Waiting on message!");
  app.wait_for_message();
}

#[cfg(target_os = "macos")]
fn main() {
  let mut app;
  match systray::Application::new() {
    Ok(w) => app = w,
    Err(_) => panic!("Can't create tray icon app!"),
  }
  // has to be absolute path?!
  let icon_path = String::from(
    env::current_dir()
      .unwrap()
      .join("examples")
      .join("rust-logo.png")
      .to_str()
      .unwrap(),
  );

  app
    .add_menu_item(&"Print a thing".to_string(), |_| {
      println!("Printing a thing!");
    })
    .ok();

  const ICON_BUFFER: &'static [u8] = include_bytes!("rust-logo.png");
  app.set_icon_from_buffer(ICON_BUFFER, 256, 256).unwrap();
  app.wait_for_message();
}

#[cfg(target_os = "windows")]
fn main() {
  let mut app;
  match systray::Application::new() {
    Ok(w) => app = w,
    Err(_) => panic!("Can't create tray icon app!"),
  }
  // has to be absolute path?!
  let icon_path = String::from(
    env::current_dir()
      .unwrap()
      .join("examples")
      .join("rust-logo.png")
      .to_str()
      .unwrap(),
  );

  app
    .add_menu_item(&"Print a thing".to_string(), |_| {
      println!("Printing a thing!");
    })
    .ok();

  //  const ICON_BUFFER: &'static [u8] = include_bytes!("rust-logo.ico");
  //  app.set_icon_from_buffer(ICON_BUFFER, 256, 256).unwrap();

  match app.set_icon_from_file(&icon_path) {
    Ok(()) => app.wait_for_message(),
    Err(SystrayError::OsError(e)) => {
      panic!("Can't create tray icon app! (OsError: {})", e)
    }
    Err(SystrayError::NotImplementedError) => {
      panic!("Can't create tray icon app! (NotImplementedError)")
    }
    Err(SystrayError::UnknownError) => {
      panic!("Can't create tray icon app! (UnknownError)")
    }
  }
}
