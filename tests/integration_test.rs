extern crate systray;

use systray::Application;

#[test]
fn it_adds_two() {
  let app = Application::new();
  assert_eq!(4, app.ok());
}
